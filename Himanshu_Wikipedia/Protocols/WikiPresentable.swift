//
//  WikiPresentable.swift
//  Himanshu_Wikipedia
//
//  Created by Himanshu on 22/07/18.
//  Copyright © 2018 Himanshu_Wikipedia. All rights reserved.
//

import UIKit

protocol WikiPresentable {
    var url: DynamicTypes<String> { get }
    var title: DynamicTypes<String> { get }
    func updateUrl(url: String)
    func updateTitle(title: String)
}
