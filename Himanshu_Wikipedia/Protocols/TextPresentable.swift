//
//  TextPresentable.swift
//  Himanshu_Wikipedia
//
//  Created by Himanshu on 22/07/18.
//  Copyright © 2018 Himanshu_Wikipedia. All rights reserved.
//

import UIKit

protocol TextPresentable {
    var searchText: DynamicTypes<String> { get }
    func updateSearchText(text: String)
    var textColor: UIColor { get }
    var font: UIFont { get }
}

extension TextPresentable {
    
    var textColor: UIColor {
        return .red
    }
    
    var font: UIFont {
        return .systemFont(ofSize: 17)
    }
    
}
