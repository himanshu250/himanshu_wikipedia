//
//  WikiSearchModel.swift
//  Himanshu_Wikipedia
//
//  Created by Himanshu on 22/07/18.
//  Copyright © 2018 Himanshu_Wikipedia. All rights reserved.
//

import Foundation

struct WikiSearchModel {
    var searchText = ""
    var searchUrl = ""
    var title = ""
}
