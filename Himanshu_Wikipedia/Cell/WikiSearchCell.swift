//
//  WikiSearchCell.swift
//  Himanshu_Wikipedia
//
//  Created by Himanshu on 22/07/18.
//  Copyright © 2018 Himanshu_Wikipedia. All rights reserved.
//

import Foundation
import UIKit

class WikiSearchCell: UITableViewCell {
    var entity: Wiki! {
        didSet {
            setupUI()
        }
    }
    @IBOutlet weak var entityImageView: UIImageView!
    @IBOutlet weak var entityDescriptionLabel: UILabel!
    @IBOutlet weak var entityTitleLabel: UILabel!
    func setupUI() {
        if let url = entity.url {
            if let imageURL = URL(string: url) {
                // Uncommnet if want to use SDWebImage
                // entityImageView.sd_setImage(with: imageURL)
                entityImageView.downloadedFrom(url: imageURL)
            }
        }
        entityTitleLabel.text = entity.title
        entityDescriptionLabel.text = entity.entityDescription ?? ""
    }
}
