//
//  WikiSearchViewController+Delegates.swift
//  Himanshu_Wikipedia
//
//  Created by Himanshu on 22/07/18.
//  Copyright © 2018 Himanshu_Wikipedia. All rights reserved.
//

import Foundation
import UIKit

extension WikiSearchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResultsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let wikiSearchCell = tableView.dequeueReusableCell(withIdentifier: "wikiSearchCell") as! WikiSearchCell
        if searchResultsArray.count > 0 {
            wikiSearchCell.entity = searchResultsArray[indexPath.row]
        }
        return wikiSearchCell
    }
}
    
extension WikiSearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if searchResultsArray.count > 0 {
            let actualTitle = searchResultsArray[indexPath.row].title!.replacingOccurrences(of: " ", with: "_")
            let url = "https://en.wikipedia.org/wiki/\(actualTitle)"
            wikiDelegate?.updateUrl(url: url)
            wikiDelegate?.updateTitle(title: actualTitle)
            let vc = WikipediaViewController.viewController(with: wikiDelegate!)
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}
