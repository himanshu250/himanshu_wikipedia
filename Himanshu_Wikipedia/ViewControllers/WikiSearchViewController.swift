//
//  WikiSearchViewController.swift
//  Himanshu_Wikipedia
//
//  Created by Himanshu on 22/07/18.
//  Copyright © 2018 Himanshu_Wikipedia. All rights reserved.
//

import UIKit
import CoreData
import Foundation
class WikiSearchViewController: UIViewController, UISearchBarDelegate {
    
    @IBOutlet weak var contentTable: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var spacingForSearchBar: NSLayoutConstraint!
    @IBOutlet weak var emptyImageStackView: UIStackView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var searchResultsArray = [Wiki]()
    var contentCellsArray = [UITableViewCell]()
    let notificationCenter = NotificationCenter.default
    var pageNumber = 1
    var delegate: TextPresentable? = WikiSearchViewModel()
    var wikiDelegate: WikiPresentable? = WikiPediaViewModel()
    
    class func viewController() -> WikiSearchViewController {
        let mainView = UIStoryboard(name: "Main", bundle: nil)
        let searchVC = mainView.instantiateViewController(withIdentifier: "searchVC") as! WikiSearchViewController
        return searchVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contentTable.dataSource = self
        contentTable.delegate = self
        searchBar.inputAccessoryView?.isHidden = true
        addDoneButtonOnKeyboard()
        contentTable.rowHeight = UITableViewAutomaticDimension
        hideCollectionViewAndIndicator()
        addNotificationObserver()
        setupDataSourceAndDelegate()
        searchBarSearchButtonClicked(searchBar)
        let _ = delegate?.searchText.bind { _ in
            return self.searchBar.text
        }
    }
    
    private func hideCollectionViewAndIndicator() {
        indicator.isHidden = true
        contentTable.isHidden = true
    }
    
    private func setupDataSourceAndDelegate() {
        searchBar.delegate = self
    }
    
    // UISearchBarDelegate
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text!.count == 0 {
            indicator.isHidden = true
            self.title = "Search"
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        indicator.isHidden = false
        contentTable.isHidden = false
        searchResultsArray.removeAll()
        getNextPage(pageNumber: 1)
        
    }
    
    public func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let actualText = (searchBar.text! as NSString).replacingCharacters(in: range, with: text) as NSString
        delegate?.updateSearchText(text: actualText as String)
        getSearchResults(for: (delegate?.searchText.value)!)
        return true
    }
    
    func getSearchResults(for searchText: String) {
        indicator.isHidden = false
        contentTable.isHidden = false
        searchResultsArray.removeAll()
        getNextPage(pageNumber: 1)
    }
    
    
    // get next Page with current page number
    private func getNextPage(pageNumber: Int) {
        if !(delegate?.searchText.value?.isEmpty)! {
            WikiManager.sharedInstance.searchForText(text: (delegate?.searchText.value)!, pageNumber: pageNumber) { [weak self](error, resultArray) in
                if let strongSelf = self {
                    guard let result = resultArray, result.count > 0 else {
                        strongSelf.showErrorAlert(message: "")
                        return
                    }
                    for record in result {
                        strongSelf.searchResultsArray.append(record)
                    }
                    if error == nil && (resultArray?.count ?? 0) > 0 {
                        DispatchQueue.main.async(execute: { () -> Void in
                            strongSelf.indicator.isHidden = true
                            strongSelf.title = strongSelf.delegate?.searchText.value
                            strongSelf.contentTable.reloadData()
                        })
                        
                    } else {
                        DispatchQueue.main.async(execute: { () -> Void in
                            strongSelf.indicator.isHidden = true
                            strongSelf.contentTable.isHidden = true
                            strongSelf.showErrorAlert(message:error?.localizedDescription ?? "")
                        })
                    }
                }
            }
        }
        else {
            contentTable.isHidden = true
            indicator.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == searchResultsArray.count - 1 {
            moreData()
        }
    }
    
    private func moreData() {
        pageNumber += 1
        getNextPage(pageNumber: pageNumber)
    }
    
    // show error
    private func showErrorAlert(message: String) {
        indicator.isHidden = true
        let alertController = UIAlertController(title: "Search", message: !message.isEmpty ? message : "No images to show." , preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "Dismiss", style: .default, handler: nil)
        alertController.addAction(dismissAction)
        present(alertController, animated: true, completion: nil)
    }
}
