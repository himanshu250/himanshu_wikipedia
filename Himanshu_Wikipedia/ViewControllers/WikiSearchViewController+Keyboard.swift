//
//  WikiSearchViewController+Keyboard.swift
//  Himanshu_Wikipedia
//
//  Created by Himanshu on 22/07/18.
//  Copyright © 2018 Himanshu_Wikipedia. All rights reserved.
//

import Foundation
import UIKit

extension WikiSearchViewController {
    
    func addNotificationObserver() {
        notificationCenter.addObserver(self, selector: #selector(WikiSearchViewController.keyboardWasShown(notification:)), name: .UIKeyboardDidShow, object: nil)
        notificationCenter.addObserver(self, selector: #selector(WikiSearchViewController.keyboardWasShown(notification:)), name: .UIKeyboardDidChangeFrame, object: nil)
        notificationCenter.addObserver(self, selector: #selector(WikiSearchViewController.keyboardWillBeHidden(notification:)), name: .UIKeyboardDidHide, object: nil)
    }
    
    @objc func hideAccessoryView(_ sender: AnyObject) {
        searchBar.resignFirstResponder()
    }
    
    @objc func keyboardWasShown(notification: Notification){
        let userinfo = notification.userInfo
        let keyboardSize = (userinfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        let edgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: (keyboardSize?.height)!, right: 0)
        contentTable.contentInset = edgeInsets
        searchBar.inputAccessoryView?.isHidden = false
    }
    
    @objc func keyboardWillBeHidden(notification: Notification){
        let zeroInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        contentTable.contentInset = zeroInsets
        searchBar.inputAccessoryView?.isHidden = true
    }
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(WikiSearchViewController.doneButtonAction))
        let items = NSMutableArray()
        items.add(done)
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        searchBar.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction() {
        searchBar.resignFirstResponder()
    }
}
