//
//  WikipediaViewController.swift
//  Himanshu_Wikipedia
//
//  Created by Himanshu on 22/07/18.
//  Copyright © 2018 Himanshu_Wikipedia. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class WikipediaViewController: UIViewController {
    @IBOutlet weak var wikiWebView: WKWebView!
    var delegate : WikiPresentable?
    
    class func viewController(with delegate: WikiPresentable) -> WikipediaViewController {
        let mainView = UIStoryboard(name: "Main", bundle: nil)
        let wikiVC = mainView.instantiateViewController(withIdentifier: "wikipediaVC") as! WikipediaViewController
        wikiVC.delegate = delegate
        return wikiVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = delegate?.title.value
        let actualUrlString = delegate?.url.value?.replacingOccurrences(of: " ", with: "_")
        if let actualUrl = URL(string: actualUrlString!) {
            wikiWebView.load(URLRequest(url: actualUrl))
            wikiWebView.allowsBackForwardNavigationGestures = true
        }
        else {
            showErrorAlert()
        }
    }
    
    private func showErrorAlert() {
        let alertController = UIAlertController(title: "Oops!", message : "Nothing to show." , preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "Dismiss", style: .default, handler: { _ in
            self.navigationController?.popViewController(animated: true)
        })
        alertController.addAction(dismissAction)
        present(alertController, animated: true, completion: nil)
    }
}
