//
//  CoreDataClient.swift
//  Wikipedia
//
//  Created by Himanshu on 20/07/18.
//  Copyright © 2018 Himanshu. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class CoreDataClient: NSObject {
    
    private override init() {}
    static let sharedInstance = CoreDataClient()
    var context: NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    func request() -> NSFetchRequest<NSFetchRequestResult> {
        NSEntityDescription.entity(forEntityName: "WikiSearch", in: context)
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "WikiSearch")
        request.returnsObjectsAsFaults = false
        return request
    }
    
    // Make use of this while retrieving core data records
    lazy var fetchedhResultController: NSFetchedResultsController<NSFetchRequestResult> = {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: Wiki.self))
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "author", ascending: true)]
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        return frc
    }()

    
    func createUserEntityFrom(dictionary: [String: AnyObject]) -> NSManagedObject? {
        let context = CoreDataClient.sharedInstance.context
        if let searchEntity = NSEntityDescription.insertNewObject(forEntityName: "Wiki", into: context) as? Wiki {
            searchEntity.title = dictionary["title"] as? String
            searchEntity.url = dictionary["thumbnail"]?["source"] as? String
            if let description = dictionary["terms"]?["description"] as? [String] {
                searchEntity.entityDescription = description[0]
            }
            return searchEntity
        }
        return nil
    }
    
    func saveInCoreDataWith(array: [[String: AnyObject]]) {
        _ = array.map{self.createUserEntityFrom(dictionary: $0)}
        do {
            try CoreDataClient.sharedInstance.context.save()
        } catch let error {
            print(error)
        }
    }
}
