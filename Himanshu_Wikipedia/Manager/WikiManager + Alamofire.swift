//
//  WikiManager + Alamofire.swift
//  Wikipedia
//
//  Created by Himanshu on 20/07/18.
//  Copyright © 2018 Himanshu. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import CoreData

extension WikiManager {
    func request(url: String, type: HTTPMethod, with completion: @escaping WikiCompletionBlock) {
        Alamofire.request(url, method: type).responseJSON { response in
            response.result.ifSuccess {
                DispatchQueue.main.async {
                        do {
                            let responseResult = response.result
                            let resultsDictionary = responseResult.value as? [String: AnyObject]
                            guard let _ = resultsDictionary, let resultsContainer = resultsDictionary!["query"] as? NSDictionary, let searchResultsArray = resultsContainer["pages"] as? [NSDictionary] else { return }
                            CoreDataClient.sharedInstance.saveInCoreDataWith(array: searchResultsArray as! [[String : AnyObject]])
                            let WikiSearchArray: [Wiki] = searchResultsArray.map { searchDictionary in
                                let WikiResult = CoreDataClient.sharedInstance.createUserEntityFrom(dictionary: searchDictionary as! [String : AnyObject])
                                return WikiResult as! Wiki
                            }
                            completion(nil, WikiSearchArray)
                        }
                }
            }
            response.result.ifFailure {
                DispatchQueue.main.async {
                    let _ = response.response?.statusCode ?? 0
                    completion(nil, nil)
                }
            }
        }
    }
    
  
}
