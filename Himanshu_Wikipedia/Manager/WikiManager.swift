//
//  WikiManager.swift
//  Wikipedia
//
//  Created by Himanshu on 20/07/18.
//  Copyright © 2018 Himanshu. All rights reserved.
//

import Foundation
import UIKit

class WikiManager {
    
    private init() {}
    static var imageURLDict:[String: UIImage] = [:]
    var currentSearchedText = ""
    var currentPageNumber = 1
    static var sharedInstance = WikiManager()
    typealias WikiCompletionBlock = (NSError?, [Wiki]?) -> Void
    let urlConfig = URLSessionConfiguration.default
    
    public func searchForText(text: String, pageNumber: Int, with completion: @escaping WikiCompletionBlock) -> Void {
        if let searchText = text.addingPercentEncoding(withAllowedCharacters:.urlHostAllowed) {
            let url = "https://en.wikipedia.org//w/api.php?action=query&format=json&prop=pageimages%7Cpageterms&generator=prefixsearch&redirects=1&formatversion=2&piprop=thumbnail&pithumbsize=50&pilimit=10&wbptterms=description&gpssearch=\(searchText)&gpslimit=\(pageNumber * 10)"
            request(url: url, type: .get) { (error, resultArray) in
                completion(nil, resultArray ?? [])
            }
        }
    }
}
