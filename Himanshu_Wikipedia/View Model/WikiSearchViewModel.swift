//
//  WikiSearchViewModel.swift
//  Himanshu_Wikipedia
//
//  Created by Himanshu on 22/07/18.
//  Copyright © 2018 Himanshu_Wikipedia. All rights reserved.
//

import Foundation
import UIKit


class WikiSearchViewModel: NSObject, TextPresentable {
    var searchModel =  WikiSearchModel()
    var searchText: DynamicTypes<String> { return DynamicTypes<String>(searchModel.searchText) }
    func updateSearchText(text: String) {
        searchModel.searchText = text
    }
    
}
